from django.db import models

# Create your models here.

#The Shoe resource should track its manufacturer, its model name, its color, 
#a URL for a picture, and the bin in the wardrobe where it exists.
class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    bin_number = models.PositiveSmallIntegerField(null=True, blank=True)
    bin_size = models.PositiveSmallIntegerField(null=True, blank=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=150)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    picture_url = models.URLField(blank=True, null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE
    )
