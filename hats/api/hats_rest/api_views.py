from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO



class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
    ]
class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style",
    ]
class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style",
        "url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats   (request, location_vo_id=None):
    
        if request.method == "GET":
                if location_vo_id is not None:
                    hats = Hat.objects.filter(location=location_vo_id)
                else:
                    hats = Hat.objects.all()
                return JsonResponse(
                    {"hats": hats},
                    encoder=HatListEncoder,
                )
        else:
                content = json.loads(request.body)

                # Get the Conference object and put it in the content dict
                try:
                    location_href = content["location"]
                    location = LocationVO.objects.get(import_href=location_href)
                    content["location"] = location
                except LocationVO.DoesNotExist:
                    return JsonResponse(
                        {"message": "Invalid location id"},
                        status=400,
                    )
                except Exception as e:
                    return JsonResponse({"message": e}
                    )
                hat = Hat.objects.create(**content)
                return JsonResponse(
                    hat,
                    encoder=HatDetailEncoder,
                    safe=False,
                )
                
def api_show_hat(request, pk):
 
    hat = Hat.objects.get(id=pk)
    return JsonResponse(
        hat,
        encoder=HatDetailEncoder,
        safe=False,
    )

# class AttendeeListEncoder(ModelEncoder):
#     model = Attendee
#     properties = ["name"]
#     def get_extra_data(self, o):
#         return {"conference": o.conference.name}

# @require_http_methods(["GET", "POST"])
# def api_list_attendees(request, conference_vo_id=None):
#     """
#     Lists the attendees names and the link to the attendee
#     for the specified conference id.

#     Returns a dictionary with a single key "attendees" which
#     is a list of attendee names and URLS. Each entry in the list
#     is a dictionary that contains the name of the attendee and
#     the link to the attendee's information.

#     {
#         "attendees": [
#             {
#                 "name": attendee's name,
#                 "href": URL to the attendee,
#             },
#             ...
#         ]
#     }
#     """
#     if request.method == "GET":
#         if conference_vo_id is not None:
#             attendees = Attendee.objects.filter(conference=conference_vo_id)
#         else:
#             attendees = Attendee.objects.all()
#         return JsonResponse(
#             {"attendees": attendees},
#             encoder=AttendeeListEncoder,
#         )
#     else:
#         content = json.loads(request.body)

#         # Get the Conference object and put it in the content dict
#         try:
#             conference_href = content["conference"]
#             conference = ConferenceVO.objects.get(import_href=conference_href)
#             content["conference"] = conference
#         except ConferenceVO.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid conference id"},
#                 status=400,
#             )

#         attendee = Attendee.objects.create(**content)
#         return JsonResponse(
#             attendee,
#             encoder=AttendeeDetailEncoder,
#             safe=False,
#         )
