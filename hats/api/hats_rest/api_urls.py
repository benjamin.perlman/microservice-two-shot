
from django.urls import path, include

from .api_views import  api_list_hats, api_show_hat

urlpatterns = [
    path("hats/", api_list_hats, name="api_create_hats"),
    path(
        "locations/<int:location_vo_id>/hats/",
        api_list_hats,
        name="api_list_hats",
    ),
    path("hats/<int:pk>/", api_show_hat, name="api_show_hat"),
]

# urlpatterns = [
 
#      # path("admin/", admin.site.urls),
#     path("api/", include("hats.api_urls")),

#     # path("locations/", api_locations, name="api_locations"),
#     # path("locations/<int:pk>/", api_location, name="api_location"),
#     # path("bins/", api_bins, name="api_bins"),
#     # path("bins/<int:pk>/", api_bin, name="api_bin"),
#]
