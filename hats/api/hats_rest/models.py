from django.db import models

#from wardrobe.api.wardrobe_api.models import Location

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    bin_number = models.PositiveSmallIntegerField(null=True, blank=True)
    bin_size = models.PositiveSmallIntegerField(null=True, blank=True)


    

class Hat(models.Model):
    """
    fabric, its style name, its color, a URL for a picture, and the location in the wardrobe where it exists.
    """
    fabric = models.CharField(max_length=50)
    style = models.CharField(max_length= 50)
    url = models.URLField(null = True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )
     
    
