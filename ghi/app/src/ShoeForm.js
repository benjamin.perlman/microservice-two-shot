import React from 'react';

class ShoeForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      manufacturer: '',
      model_name: '',
      picture_url: '',
      color: '',
      bin: '',
      bins: [],
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeBin = this.handleChangeBin.bind(this);
    this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this);
    this.handleChangeModelName = this.handleChangeModelName.bind(this);
    this.handleChangeColor = this.handleChangeColor.bind(this);
    this.handleChangePictureUrl = this.handleChangePictureUrl.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ bins: data.bins });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    //data.model_name = data.modelName;
    //data.picture_url = data.pictureUrl;
    //delete data.modelName;
    //delete data.pictureUrl;
    delete data.bins;
    console.log(data);

    const shoeUrl = 'http://localhost:8080/api/shoes/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const shoeResponse = await fetch(shoeUrl, fetchOptions);
    if (shoeResponse.ok) {
      this.setState({
        bin: '',
        model_name: '',
        manufacturer: '',
        picture_url: '',
        color: '',
      });
      console.log(shoeResponse);
    }
  }

  handleChangeBin(event) {
    const value = event.target.value;
    this.setState({ bin: value });
  }

  handleChangeModelName(event) {
    const value = event.target.value;
    this.setState({model_name: value });
  }

  handleChangeManufacturer(event) {
    const value = event.target.value;
    this.setState({ manufacturer: value });
  }
  handleChangeColor(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }
  handleChangePictureUrl(event) {
    const value = event.target.value;
    this.setState({ picture_url: value });
  }

  render() {
    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (this.state.bins.length > 0) {
      spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
      dropdownClasses = 'form-select';
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';

    return (
      <div className="my-5 container">
        <div className="row">
          <div className="col col-sm-auto">
            <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" />
          </div>
          <div className="col">
            <div className="card shadow">
              <div className="card-body">
                <form className={formClasses} onSubmit={this.handleSubmit} id="create-shoe-form">
                  <h1 className="card-title">Create A Shoe</h1>
                  <p className="mb-3">
                    Please choose which bin you want your shoes to go to.
                  </p>
                  <div className={spinnerClasses} id="loading-bin-spinner">
                    <div className="spinner-grow text-secondary" role="status">
                      <span className="visually-hidden">Loading...</span>
                    </div>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleChangeBin} name="bin" id="bin" className={dropdownClasses} required>
                      <option value="">Choose a bin</option>
                      {this.state.bins.map(bin => {
                        return (
                          <option key={bin.href} value={bin.href}>{bin.bin_number}</option>
                        )
                      })}
                    </select>
                  </div>
                  <p className="mb-3">
                    Now, tell us about your shoe.
                  </p>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={this.handleChangeModelName} required placeholder="Model Name" type="text" id="model_name" name="model_name" className="form-control" />
                        <label htmlFor="name">Name</label>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={this.handleChangeManufacturer} required placeholder="Manufacturer Name" type="text" id="manufacturer" name="manufacturer" className="form-control" />
                        <label htmlFor="manufacturer">Manufacturer</label>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={this.handleChangeColor} required placeholder="Shoe Color" type="text" id="color" name="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                      </div>
                    </div>
                    <div className= "form floating mb-3">
                        <input onChange={this.handleChangePictureUrl} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                        <label htmlFor="picture_url"></label>
                    </div>
                  </div>
                  <button className="btn btn-lg btn-primary">Create?</button>
                </form>
                <div className={messageClasses} id="success-message">
                  Congratulations! You made a shoe!
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ShoeForm;